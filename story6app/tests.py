from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .models import statusModels
from .forms import statusForms
from .views import homepage
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Story6Test(TestCase):
    def test_story_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_story_6_using_homepage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')
    
    def test_story_6_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)
    
    def test_story_6_validation_for_blank(self):
        form = statusForms(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )
    
    def test_story_6_text_is_exist(self):
        response = Client().get('/')
        self.assertContains(response, "Halo, apa kabar?")
    
    def test__story_6_models(self):
        data = statusModels(status = 'status')
        data.save()
        response = Client().get('/')    
        self.assertContains(response, 'status')

    def test_story_6_status_forms(self):
        data = {'status' : 'test'}
        form = statusForms(data=data)
        self.assertTrue(form.is_valid())
    
    def test_story_6_add_status(self):
        data = {"status" : "test"}
        response = Client().post('/', data)
        self.assertEqual(response.status_code, 302)

class Story6FuncTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FuncTest, self).setUp()

    def tearDown(self):
        self.browser.refresh()
        self.browser.quit()
        super(Story6FuncTest, self).tearDown()

    def test_input(self):
        self.browser.get(self.live_server_url)
        status = self.browser.find_element_by_id('id_status')
        status.send_keys("Coba Coba")
        time.sleep(2)
        submit = self.browser.find_element_by_id('submit')
        submit.send_keys(Keys.RETURN)
        time.sleep(2)
        self.browser.get(self.live_server_url)
        time.sleep(2)
        self.assertIn('Coba Coba', self.browser.page_source)
