from django.db import models
from datetime import datetime

# Create your models here.
class statusModels(models.Model):
    status = models.CharField(max_length=300)
    jam = models.DateTimeField(default= datetime.now)

def __str__(self):
    return self.status