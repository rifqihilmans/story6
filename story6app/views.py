from django.shortcuts import render, redirect
from .models import statusModels
from . import forms

# Create your views here.
def homepage(request):
    if request.method == 'POST':
        status_form = forms.statusForms(request.POST)
        if status_form.is_valid():
            status = status_form.save(commit=False)
            status.save()
            return redirect('/')
    else:
        status_form = forms.statusForms()
    semua_status = statusModels.objects.all()
    context = {
        'status_form': status_form,
        'semua_status': semua_status,
    }
    return render(request, 'homepage.html', context)
