from django import forms
from .models import statusModels

class statusForms(forms.ModelForm):
    class Meta:
        model = statusModels
        fields = ['status']
        widgets = {
            'status': forms.TextInput(attrs = {'class': 'form-control'}),
        }